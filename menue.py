import pygame
import sys
from pygame.locals import *


def drawMenue(screen, list, selection):
    screen.fill((0,0,0))
    background = pygame.Surface(screen.get_size())
    sizeX = background.get_width()
    sizeY = background.get_height()

    fontsize = min(72, sizeY / len(list) / 2)

    font = pygame.font.Font(None, fontsize)

    i = 1
    for item in list:
        color = (0, 130, 0)
        if i == selection:
            color = (0, 250, 0)
        text = font.render(item, 1, color)
        textpos = text.get_rect()
        textpos.centerx = background.get_rect().centerx
        textpos.centery = 360 + fontsize * (i-len(list) / 2)
        screen.blit(text, textpos)
        i += 1

    for i in range ( 0, sizeX, 25 ):
      pygame.draw.line ( screen, ( 0, 250, 0 ), ( 0, i ), ( i, sizeY ), 1 )
      pygame.draw.line ( screen, ( 0, 250, 0 ), ( i, 0 ), ( sizeX, i ), 1 )
      pygame.draw.line ( screen, ( 0, 250, 0 ), ( sizeX - i, 0 ), ( 0, i ), 1 )
      pygame.draw.line ( screen, ( 0, 250, 0 ), ( i, sizeY ), ( sizeX, sizeY - i ), 1 )

    pygame.display.flip()



def main():


    pygame.display.init()
    pygame.font.init()

    sizeX = 1024
    sizeY = 768

    #screen = pygame.display.set_mode ( [sizeX,sizeY], pygame.FULLSCREEN )
    screen = pygame.display.set_mode ( [sizeX,sizeY] )

    list = sys.argv
    list = ["Pacman", "SuperTux", "Duke Nukem 3D", "Commander Keen", "Test1", "Test2", "Pacman", "SuperTux", "Duke Nukem 3D", "Commander Keen", "Test1", "Test2"]
    list.pop(0)
    selection = 1

    drawMenue(screen, list, selection)

    while 1:
        for event in pygame.event.get():
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_ESCAPE:
                    pygame.quit()
                    sys.exit(0)

                if event.key == pygame.K_RETURN:
                    pygame.quit()
                    sys.exit(selection)

                if event.key == pygame.K_DOWN:
                    selection += 1
                    if selection > len(list):
                        selection = len(list)
                    drawMenue(screen, list, selection)

                if event.key == pygame.K_UP:
                    selection -= 1
                    if selection < 1:
                        selection = 1
                    drawMenue(screen, list, selection)


        pygame.time.wait(100)

if __name__ == '__main__': main()