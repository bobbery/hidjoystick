#define F_CPU   12000000L    

#include "hiddescriptor.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/wdt.h>

#include "usbdrv.h"

/* pin assignments:

PB1	Key 1
PB2	Key 2
PB3	Key 3
PB4	Key 4

PD0	USB-
PD1	debug tx
PD2	USB+ (int0)
PD6	Key Shift
*/

static void hardwareInit(void)
{
    uchar	i, j;

    PORTB = 0xff;   /* activate all pull-ups */
    DDRB = 0x01;       /* all pins input */
    PORTD = 0xfa;   /* 1111 1010 bin: activate pull-ups except on USB lines */
    DDRD = 0x07;    /* 0000 0111 bin: all pins input except USB (-> USB reset) */
    j = 0;
    while(--j)
    {    
        i = 0;
        while(--i); /* delay >10ms for USB reset */
    }
    DDRD = 0x02;    /* 0000 0010 bin: remove USB reset condition */
    
    /* configure timer 0 for a rate of 12M/(1024 * 256) = 45.78 Hz (~22ms) */
    TCCR0 = 5;      /* timer 0 prescaler: 1024 */
}



typedef struct {
    uint8_t modifier;
    uint8_t reserved;
    uint8_t keycode[6];
} keyboard_report_t;

static keyboard_report_t keyboard_report; // sent to PC
static uchar    idleRate;           // repeat rate for keyboards
 
#define KEY_ENTER 0x28
#define KEY_ESC 0x29
#define KEY_SPACE 0x2C
#define KEY_RIGHT 0x4F
#define KEY_LEFT 0x50
#define KEY_DOWN 0x51
#define KEY_UP 0x52
#define KEY_CONTROL 0xE0


usbMsgLen_t usbFunctionSetup(uchar data[8]) {
    usbRequest_t *rq = (void *)data;

    if((rq->bmRequestType & USBRQ_TYPE_MASK) == USBRQ_TYPE_CLASS) {
        switch(rq->bRequest) {
        case USBRQ_HID_GET_REPORT: // send "no keys pressed" if asked here
            // wValue: ReportType (highbyte), ReportID (lowbyte)
            usbMsgPtr = (void *)&keyboard_report; // we only have this one
            keyboard_report.modifier = 0;
            keyboard_report.keycode[0] = 0;
            return sizeof(keyboard_report);
        case USBRQ_HID_SET_REPORT: // if wLength == 1, should be LED state
            return (rq->wLength.word == 1) ? USB_NO_MSG : 0;
        case USBRQ_HID_GET_IDLE: // send idle rate to PC as required by spec
            usbMsgPtr = &idleRate;
            return 1;
        case USBRQ_HID_SET_IDLE: // save idle rate as required by spec
            idleRate = rq->wValue.bytes[1];
            return 0;
        }
    }
    
    return 0; // by default don't return any data
}

#define LEDOFF          PORTB |= (1<<0)
#define LEDON           PORTB &= ~(1<<0)
#define LEDTOG          PORTB ^= (1<<0)

void buildKeyReport(keyboard_report_t *report)
{
    if (!(PINB & (1<<PINB1)))
    {
        report->keycode[0] = KEY_UP;
    }
    else if (!(PINB & (1<<PINB2)))
    {
        report->keycode[0] = KEY_DOWN;
    }
    else
    {
        report->keycode[0] = 0;
    }

    if (!(PINB & (1<<PINB3)))
    {
        report->keycode[1] = KEY_LEFT;
    }
    else if (!(PINB & (1<<PINB4)))
    {
        report->keycode[1] = KEY_RIGHT;
    }
    else
    {
        report->keycode[1] = 0;
    }
}


int	main(void)
{
    uchar idleCounter = 0;

    wdt_enable(WDTO_2S);
    hardwareInit();
    usbInit();
    sei();
    
    for(uchar i=0; i<sizeof(keyboard_report); i++) 
    {
        ((uchar *)&keyboard_report)[i] = 0;
    }

    for(;;)
    {	
        wdt_reset();
        usbPoll();
        
        if(TIFR & (1<<TOV0))
        {     
            TIFR = 1<<TOV0;
            if(idleRate != 0)
            {
                if(idleCounter > 4)
                {
                    idleCounter -= 5;   /* 22 ms in units of 4 ms */
                }
                else
                {
                    idleCounter = idleRate;
                }
            }
        }
        
        if(usbInterruptIsReady())
        {
            LEDTOG;
            
            buildKeyReport(&keyboard_report);
            
            usbSetInterrupt((void *)&keyboard_report, sizeof(keyboard_report));
        }
    }
    return 0;
}

